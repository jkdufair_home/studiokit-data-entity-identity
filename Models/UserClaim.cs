﻿using Microsoft.AspNet.Identity.EntityFramework;
using StudioKit.Data.Interfaces;
using System;

namespace StudioKit.Data.Entity.Identity.Models
{
	public class UserClaim : IdentityUserClaim, IAuditable
	{
		public string ClaimValueType { get; set; }

		public string ClaimIssuer { get; set; }

		public string ClaimOriginalIssuer { get; set; }

		public DateTime DateStored { get; set; }

		public DateTime DateLastUpdated { get; set; }

		public string LastUpdatedById { get; set; }
	}
}