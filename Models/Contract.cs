﻿using StudioKit.Data.Entity.Identity.Interfaces;
using System.ComponentModel.DataAnnotations;

namespace StudioKit.Data.Entity.Identity.Models
{
	public class Contract : ModelBase
	{
		[Required]
		public string UserId { get; set; }

		public virtual IUser User { get; set; }

		[Required]
		public int UserAgreementId { get; set; }

		public virtual UserAgreement UserAgreement { get; set; }
	}
}