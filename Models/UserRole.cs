﻿using System;
using Microsoft.AspNet.Identity.EntityFramework;
using StudioKit.Data.Interfaces;

namespace StudioKit.Data.Entity.Identity.Models
{
	public class UserRole : IdentityUserRole, IAuditable
	{
		public DateTime DateStored { get; set; }

		public DateTime DateLastUpdated { get; set; }

		public string LastUpdatedById { get; set; }
	}
}