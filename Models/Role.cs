﻿using System;
using Microsoft.AspNet.Identity.EntityFramework;
using StudioKit.Data.Interfaces;

namespace StudioKit.Data.Entity.Identity.Models
{
	public class Role : IdentityRole<string, UserRole>, IAuditable
	{
		public Role()
		{
			Id = Guid.NewGuid().ToString();
		}

		public Role(string roleName) : this()
		{
			Name = roleName;
		}

		public DateTime DateStored { get; set; }

		public DateTime DateLastUpdated { get; set; }

		public string LastUpdatedById { get; set; }
	}
}