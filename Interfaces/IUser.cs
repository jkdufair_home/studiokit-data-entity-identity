﻿using StudioKit.Data.Interfaces;

namespace StudioKit.Data.Entity.Identity.Interfaces
{
	/// <summary>
	/// Defines a user in the application where UserName is ePPN (eduPersonPrincipalName) from Shibboleth
	/// https://www.purdue.edu/apps/account/docs/Shibboleth/Shibboleth_information.jsp
	/// </summary>
	public interface IUser : Microsoft.AspNet.Identity.IUser<string>, IAuditable
	{
		/// <summary>
		/// Shibboleth "uid", equivalent to CareerAccountAlias for Purdue.
		/// </summary>
		string Uid { get; set; }

		string CareerAccountAlias { get; set; }

		/// <summary>
		/// Shibboleth "employeeNumber", equivalent to PUID for Purdue.
		/// </summary>
		string EmployeeNumber { get; set; }

		string Puid { get; set; }

		string FirstName { get; set; }

		string LastName { get; set; }

		string FullName { get; }

		string Email { get; set; }
	}
}