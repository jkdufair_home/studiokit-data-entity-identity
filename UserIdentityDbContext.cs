using Microsoft.AspNet.Identity.EntityFramework;
using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.Data.Entity.Identity.Models;
using System.Data.Common;
using System.Data.Entity;

namespace StudioKit.Data.Entity.Identity
{
	/// <summary>
	/// Subclass of <see cref="IdentityDbContext"/> with TUser, TIdentityProvider, and other Identity framework entities
	/// </summary>
	/// <typeparam name="TUser">Type of <see cref="IUser"/></typeparam>
	/// <typeparam name="TIdentityProvider">Type of <see cref="IdentityProvider"/></typeparam>
	public class UserIdentityDbContext<TUser, TIdentityProvider>
		: IdentityDbContext<TUser, Role, string, UserLogin, UserRole, UserClaim>,
			IUserIdentityDbContext<TUser, TIdentityProvider>
		where TUser : IdentityUser<string, UserLogin, UserRole, UserClaim>, IUser
		where TIdentityProvider : IdentityProvider, new()
	{
		public UserIdentityDbContext() : this("DefaultConnection")
		{
		}

		public UserIdentityDbContext(DbConnection existingConnection) : base(existingConnection, true)
		{
		}

		public UserIdentityDbContext(string nameOrConnectionString)
			: base(nameOrConnectionString)
		{
		}

		public virtual DbSet<UserRole> IdentityUserRoles { get; set; }

		public virtual DbSet<UserLogin> IdentityUserLogins { get; set; }

		public virtual DbSet<UserClaim> IdentityUserClaims { get; set; }

		public virtual DbSet<TIdentityProvider> IdentityProviders { get; set; }
	}
}