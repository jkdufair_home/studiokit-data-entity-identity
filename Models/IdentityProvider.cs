﻿using System.ComponentModel.DataAnnotations;

namespace StudioKit.Data.Entity.Identity.Models
{
	public class IdentityProvider : ModelBase
	{
		[Required]
		public string Name { get; set; }

		[Required]
		public string LoginUrl { get; set; }

		[Required]
		public string LogoutUrl { get; set; }

		public string MobileLoginUrl { get; set; }

		public string Text { get; set; }

		public byte[] Image { get; set; }
	}
}