﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace StudioKit.Data.Entity.Identity.Models
{
	public class UserAgreement : ModelBase
	{
		[Required]
		public string Text { get; set; }

		public virtual ICollection<Contract> Contracts { get; set; }
	}
}