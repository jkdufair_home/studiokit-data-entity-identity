﻿using System.Collections.Generic;

namespace StudioKit.Data.Entity.Identity
{
	public static class BaseRole
	{
		public const string SuperAdmin = "SuperAdmin";
		public const string Admin = "Admin";
		public const string Creator = "Creator";

		public static List<string> GlobalRoles = new List<string> { SuperAdmin, Admin, Creator };

		public const string GroupOwner = "GroupOwner";
		public const string GroupLearner = "GroupLearner";
		public const string GroupGrader = "GroupGrader";

		public static List<string> GroupRoles = new List<string> { GroupOwner, GroupLearner, GroupGrader };
	}
}