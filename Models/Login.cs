﻿using StudioKit.Data.Entity.Identity.Interfaces;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace StudioKit.Data.Entity.Identity.Models
{
	public class Login : ModelBase
	{
		[Required]
		public string UserId { get; set; }

		[ForeignKey(nameof(UserId))]
		[IgnoreDataMember]
		public virtual IUser User { get; set; }

		[Required]
		public string UserAgent { get; set; }

		public string IpAddress { get; set; }
	}
}