﻿using Microsoft.AspNet.Identity.EntityFramework;
using StudioKit.Data.Entity.Identity.Models;
using System.Data.Entity;

namespace StudioKit.Data.Entity.Identity.Interfaces
{
	/// <summary>
	/// Interface for an <see cref="IdentityDbContext"/> with TUser, TIdentityProvider, and other Identity framework entities
	/// </summary>
	/// <typeparam name="TUser">Type of <see cref="IUser"/></typeparam>
	/// <typeparam name="TIdentityProvider">Type of <see cref="IdentityProvider"/></typeparam>
	public interface IUserIdentityDbContext<TUser, TIdentityProvider>
		where TUser : IdentityUser<string, UserLogin, UserRole, UserClaim>, IUser
		where TIdentityProvider : IdentityProvider, new()
	{
		DbSet<UserRole> IdentityUserRoles { get; set; }

		DbSet<UserLogin> IdentityUserLogins { get; set; }

		DbSet<UserClaim> IdentityUserClaims { get; set; }

		DbSet<TIdentityProvider> IdentityProviders { get; set; }
	}
}