﻿using StudioKit.Data.Entity.Identity.Interfaces;
using System.ComponentModel.DataAnnotations;

namespace StudioKit.Data.Entity.Identity.Models
{
	/// <summary>
	/// An abstract base class for user defined settings.
	/// The existence of a row equates to the user having defined the setting.
	/// </summary>
	public abstract class UserSetting : ModelBase
	{
		[Required]
		public string UserId { get; set; }

		public virtual IUser User { get; set; }
	}

	public class DisableEmailSetting : UserSetting
	{
	}

	// Temporary: needed a second subclass to make the "Discriminator" column generate in the db
	public class OtherSetting : UserSetting
	{
	}
}